/**
 * Test class to illustrate polymorphic behavior at runtime
 * 
 * @author username@email.uscb.edu
 * @version CSCI 146 HW2 Lab part 1
 */
public class RuntimePolymorphismDemo 
{
	public static void main( String[] args )
	{
		// Here we create a 4-element, 1-D array of ...
		
		// ...and to each ... variable, we assign a reference
		// to a newly instantiated object of each ...
		
		
		/*
		 * Now, we will loop through the animals array... note that nowhere
		 * in the for loop do we ...
		 */
		
		{
			// ...
			
			
			/*
			 * (NOTE: In contrast, ... polymorphism is just another 
			 *  way of saying "...". In ... polymorphism, we're 
			 *  not ... methods -- rather, we are ... them!
			 *  Also note that ... methods have the ...
			 *  as the methods that they are over-riding. In contrast, 
			 *  ...)
			 */
			
		} // end for 
		
	} // end method main
	
} // end test class RuntimePolymorphismDemo
